module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		"bundle": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = global["webpackJsonp"] = global["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./app.js","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var nativescript_camera__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("nativescript-camera");
/* harmony import */ var nativescript_camera__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_camera__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("nativescript-imagepicker");
/* harmony import */ var nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var tns_core_modules_image_source__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("tns-core-modules/image-source");
/* harmony import */ var tns_core_modules_image_source__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_image_source__WEBPACK_IMPORTED_MODULE_3__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





const geoLocation = __webpack_require__("nativescript-geolocation");

const connectivityModule = __webpack_require__("tns-core-modules/connectivity");

/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    takePicture() {
      this.image64 = [];
      this.currentImg = [];
      this.visible = false;
      nativescript_camera__WEBPACK_IMPORTED_MODULE_0__["requestPermissions"]().then(() => {
        nativescript_camera__WEBPACK_IMPORTED_MODULE_0__["takePicture"]({
          keepAspectRatio: true,
          saveToGallery: false
        }).then(imageAsset => {
          new tns_core_modules_image_source__WEBPACK_IMPORTED_MODULE_3__["ImageSource"]().fromAsset(imageAsset).then(source => {
            const base64image = source.toBase64String("jpeg", 50);
            this.image64.push("data:image/jpeg;base64," + base64image);
          });
          this.currentImg.push(imageAsset);
          this.visible = true;
        }).catch(error => {
          this.image64 = [];
          this.currentImg = [];
          this.visible = false;
          console.log("Error -> " + error.message);
        });
      }).catch(error => {
        console.log("Error -> " + error.message);
      });
    },

    selectPicture() {
      this.image64 = [];
      this.currentImg = [];
      this.visible = false;
      let context = nativescript_imagepicker__WEBPACK_IMPORTED_MODULE_1__["create"]({
        mode: "multiple",
        mediaType: 1,
        minimumNumberOfSelection: 1,
        maximumNumberOfSelection: 11,
        showsNumberOfSelectedAssets: true
      });
      context.authorize().then(function () {
        return context.present();
      }).then(selection => {
        selection.forEach(selected => {
          new tns_core_modules_image_source__WEBPACK_IMPORTED_MODULE_3__["ImageSource"]().fromAsset(selected).then(source => {
            const base64image = source.toBase64String("jpeg", 50);
            this.image64.push("data:image/jpeg;base64," + base64image);
          });
          this.currentImg.push(selected);
          this.visible = true;
        });
      }).catch(function (error) {
        this.image64 = [];
        this.currentImg = [];
        this.visible = false;
        console.log("Error -> " + error.message);
      });
    },

    enableLocationServices: function enableLocationServices() {
      geoLocation.isEnabled().then(enabled => {
        if (!enabled) {
          geoLocation.enableLocationRequest().then(() => this.showLocation());
        } else {
          this.showLocation();
        }
      }).catch(function (error) {
        console.log("Error -> " + error.message);
      });
    },
    showLocation: function showLocation() {
      geoLocation.watchLocation(location => {
        this.currentGeoLocation = location;
      }, error => {
        console.log("Error -> " + error.message);
      }, {
        desiredAccuracy: 3,
        updateDistance: 10,
        minimumUpdateTime: 1000 * 1
      });
    },

    uploadPictureCloudinary() {
      let CLOUDINARY_URL = "https://api.cloudinary.com/v1_1/djfa7u4wc/image/upload";
      let CLOUDINARY_UPLOAD_PRESET = "hqquzoc1";
      var self = this;
      this.image64.forEach(image => {
        let formData = new FormData();
        formData.append("file", image);
        formData.append("upload_preset", CLOUDINARY_UPLOAD_PRESET);
        axios__WEBPACK_IMPORTED_MODULE_2__({
          url: CLOUDINARY_URL,
          method: "POST",
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type",
            "Content-Type": "application/x-www-form-urlencoded"
          },
          data: formData
        }).then(response => {
          self.url.push(response.data.secure_url);
        }).catch(error => {
          alert("There is an error: " + error.response);
        });
      });
    },

    uploadPicture() {
      let idSeries = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      this.getConnectivity();

      if (this.connectivity) {
        if (this.description !== "") {
          // PROGRESS BAR///////////////////
          let percent = 0;
          let intervalId = setInterval(() => {
            this.setProgressbarWidth(percent);
            percent++;

            if (percent > 100) {
              clearInterval(intervalId);
            }
          }, 100); // DISABLE CONTROLS /////////////////////////////

          this.disabled = false; // CLOUDINARY UPLOAD ///////////////////////////////

          this.uploadPictureCloudinary(); // NEW THIS INSTANCE////////////////////////

          var self = this; // DATABASE UPLOAD/////////////////////

          setTimeout(function upload() {
            self.url.forEach(url => {
              let URL = "https://apimobilegeoquizz.pagekite.me/photos";
              let formData = new FormData();
              formData.append("description", self.description);
              formData.append("lat", self.currentGeoLocation.latitude);
              formData.append("lng", self.currentGeoLocation.longitude);
              formData.append("url", url);

              if (idSeries !== 0) {
                formData.append("idSeries", idSeries);
              }

              axios__WEBPACK_IMPORTED_MODULE_2__({
                url: URL,
                method: "POST",
                crossDomain: true,
                withCredentials: true,
                headers: {
                  "Access-Control-Allow-Origin": "*",
                  "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
                  "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type",
                  "Cache-Control": "no-cache",
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                data: formData
              }).then(response => {
                self.busy = false;
                self.url = [];
                self.image64 = [];
                self.currentImg = [];
                self.description = "";
                self.city = "";
                self.showLocation();
                self.disabled = true;
                self.visible = false;
                self.svisible = false;
                console.log(response);
              }).catch(error => {
                self.showLocation();
                console.log(error);
              });
            });
            alert('Photo(s) added successfully');
          }, 10000);
        } else {
          alert("You need to write a description");
        }
      } else {
        alert("You don't have internet connection!");
      }
    },

    setProgressbarWidth(percent) {
      this.columns = percent + "*," + (100 - percent) + "*";
    },

    getConnectivity() {
      const myConnectionType = connectivityModule.getConnectionType();

      switch (myConnectionType) {
        case connectivityModule.connectionType.none:
          // Denotes no Internet connection.
          console.log("No connection");
          this.connectivity = false;
          break;

        case connectivityModule.connectionType.wifi:
          // Denotes a WiFi connection.
          console.log("WiFi connection");
          this.connectivity = true;
          break;

        case connectivityModule.connectionType.mobile:
          // Denotes a mobile connection, i.e. cellular network or WAN.
          console.log("Mobile connection");
          this.connectivity = true;
          break;

        case connectivityModule.connectionType.ethernet:
          // Denotes a ethernet connection.
          console.log("Ethernet connection");
          this.connectivity = true;
          break;
        // Bluetooth functionality in master branch (to be released with 5.0.0)
        // case connectionType.bluetooth:
        //     // Denotes a ethernet connection.
        //     console.log("Bluetooth connection");
        //    //     break;

        default:
          break;
      }
    },

    uploadSeries() {
      if (this.currentImg.length < 10) {
        alert('You need to upload at least 10 photos to create a series');
      } else {
        this.getConnectivity();

        if (this.connectivity) {
          if (this.city !== "") {
            let URL = "https://apimobilegeoquizz.pagekite.me/series";
            let formData = new FormData();
            formData.append("city", this.city);
            formData.append("lat", this.currentGeoLocation.latitude);
            formData.append("lng", this.currentGeoLocation.longitude);
            formData.append("zoom", 15);
            axios__WEBPACK_IMPORTED_MODULE_2__({
              url: URL,
              method: "POST",
              crossDomain: true,
              withCredentials: true,
              headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type",
                "Cache-Control": "no-cache",
                "Content-Type": "application/x-www-form-urlencoded"
              },
              data: formData
            }).then(response => {
              this.uploadPicture(response.data.series.id);
              alert(response.data.message);
            }).catch(error => {
              this.showLocation();
              alert(error);
            });
          } else {
            alert("You need to write a city name");
          }
        } else {
          alert("You don't have internet connection!");
        }
      }
    }

  },

  mounted() {
    this.enableLocationServices();
  },

  data() {
    return {
      url: [],
      image64: [],
      currentImg: [],
      columns: 0,
      idSerDef: null,
      city: "",
      description: "",
      busy: false,
      svisible: false,
      visible: false,
      disabled: true,
      connectivity: true,
      currentGeoLocation: {
        latitude: null,
        longitude: null,
        altitude: null,
        direction: null
      }
    };
  }

});

/***/ }),

/***/ "../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=style&index=0&id=763db97b&scoped=true&lang=css&":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.my-button[data-v-763db97b] {\n    font-family: FontAwesome;\n    font-size: 24;\n}\n.progressbar[data-v-763db97b] {\n    height: 15;\n    margin: 10;\n    border-radius: 10;\n    border-color: black;\n    border-width: 1;\n}\n.progressbar-value[data-v-763db97b] {\n    background: #006968;\n    border-radius: 9;\n}\n", ""]);

// exports

    const application = __webpack_require__("tns-core-modules/application");
    __webpack_require__("tns-core-modules/ui/styling/style-scope");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=template&id=763db97b&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "Page",
    { staticClass: "page" },
    [
      _c("ActionBar", {
        staticClass: "action-bar",
        attrs: { title: "GéoQuizz Image Uploader" }
      }),
      _c(
        "ScrollView",
        [
          _c(
            "FlexboxLayout",
            {
              attrs: {
                alignItems: "stretch",
                flexDirection: "column",
                justifyContent: "space-around"
              }
            },
            [
              _c(
                "ScrollView",
                { attrs: { orientation: "horizontal" } },
                [
                  _c(
                    "FlexboxLayout",
                    { attrs: { justifyContent: "space-around" } },
                    _vm._l(_vm.currentImg, function(img, index) {
                      return _c("Image", {
                        key: index,
                        attrs: { src: img, height: "300", margin: "2" }
                      })
                    })
                  )
                ],
                1
              ),
              _c(
                "GridLayout",
                {
                  staticClass: "progressbar",
                  attrs: {
                    columns: _vm.columns,
                    visibility: _vm.disabled ? "collapsed" : "visible",
                    alignSelf: "center"
                  }
                },
                [
                  _c("StackLayout", {
                    staticClass: "progressbar-value",
                    attrs: { col: "0" }
                  })
                ],
                1
              ),
              _c(
                "FlexboxLayout",
                {
                  attrs: {
                    alignItems: "center",
                    flexDirection: "row",
                    justifyContent: "space-around"
                  }
                },
                [
                  _c("Image", {
                    attrs: {
                      isEnabled: _vm.disabled,
                      src:
                        "https://image.flaticon.com/icons/png/512/3/3901.png",
                      width: "15%"
                    },
                    on: { tap: _vm.takePicture }
                  }),
                  _c("Image", {
                    attrs: {
                      isEnabled: _vm.disabled,
                      src:
                        "https://image.flaticon.com/icons/png/512/61/61118.png",
                      width: "15%"
                    },
                    on: { tap: _vm.selectPicture }
                  })
                ],
                1
              ),
              _c("Button", {
                staticClass: "btn btn-outline btn-active",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility:
                    !_vm.svisible && !_vm.visible ? "visible" : "collapsed",
                  text: "Create a new Series"
                },
                on: {
                  tap: function($event) {
                    _vm.svisible = true
                  }
                }
              }),
              _c("TextField", {
                staticClass: "input",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility: _vm.svisible ? "visible" : "collapsed",
                  hint: "Enter a city for the series",
                  width: "90%",
                  text: _vm.city
                },
                on: {
                  textChange: function($event) {
                    _vm.city = $event.value
                  }
                }
              }),
              _c("TextField", {
                staticClass: "input",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility:
                    _vm.visible || _vm.svisible ? "visible" : "collapsed",
                  hint: "Enter a description",
                  width: "90%",
                  text: _vm.description
                },
                on: {
                  textChange: function($event) {
                    _vm.description = $event.value
                  }
                }
              }),
              _c("Button", {
                staticClass: "btn btn-outline btn-active",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility:
                    _vm.visible && !_vm.svisible ? "visible" : "collapsed",
                  text: "Upload Picture"
                },
                on: {
                  tap: function($event) {
                    _vm.uploadPicture(0)
                  }
                }
              }),
              _c("Button", {
                staticClass: "btn btn-outline btn-active",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility:
                    _vm.currentImg.length >= 10 && _vm.svisible === false
                      ? "visible"
                      : "collapsed",
                  text: "Create a new series with the current photos"
                },
                on: {
                  tap: function($event) {
                    _vm.svisible = true
                  }
                }
              }),
              _c("Button", {
                staticClass: "btn btn-outline btn-active",
                attrs: {
                  isEnabled: _vm.disabled,
                  visibility: _vm.svisible ? "visible" : "collapsed",
                  text: "Upload series"
                },
                on: { tap: _vm.uploadSeries }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	var e = new Error("Cannot find module '" + req + "'");
	e.code = 'MODULE_NOT_FOUND';
	throw e;
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$";

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../node_modules/css-loader/index.js?!../node_modules/nativescript-theme-core/css/forest.css"), "");

// module
exports.push([module.i, "/*\r\nIn NativeScript, the app.css file is where you place CSS rules that\r\nyou would like to apply to your entire application. Check out\r\nhttp://docs.nativescript.org/ui/styling for a full list of the CSS\r\nselectors and properties you can use to style UI components.\r\n\r\n/*\r\nIn many cases you may want to use the NativeScript core theme instead\r\nof writing your own CSS rules. For a full list of class names in the theme\r\nrefer to http://docs.nativescript.org/ui/theme.\r\nThe imported CSS rules must precede all other types of rules.\r\n*/\r\n\r\n.con{\r\n    font-family: \"clique-icons\";\r\n}", ""]);

// exports

    const application = __webpack_require__("tns-core-modules/application");
    __webpack_require__("tns-core-modules/ui/styling/style-scope");

    exports.forEach(cssExport => {
        if (cssExport.length > 1 && cssExport[1]) {
            // applying the second item of the export as it contains the css contents
            application.addCss(cssExport[1]);
        }
    });
    ;
    if (false) {}


/***/ }),

/***/ "./app.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("nativescript-vue");
/* harmony import */ var nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_HelloWorld__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/HelloWorld.vue");

            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (false) {}
        
            const context = __webpack_require__("./ sync recursive (root|page)\\.(xml|css|js|ts|scss)$");
            global.registerWebpackModules(context);
            
        __webpack_require__("tns-core-modules/bundle-entry-points");
        
 // Uncommment the following to see NativeScript-Vue output logs
// Vue.config.silent = false;

nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default.a.registerElement('RadSideDrawer', () => __webpack_require__("nativescript-ui-sidedrawer").RadSideDrawer);
new nativescript_vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  render: h => h('frame', [h(_components_HelloWorld__WEBPACK_IMPORTED_MODULE_1__["default"])])
}).$start();
    
        
        
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/nativescript-dev-webpack/node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./components/HelloWorld.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./components/HelloWorld.vue?vue&type=template&id=763db97b&scoped=true&");
/* harmony import */ var _HelloWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./components/HelloWorld.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./components/HelloWorld.vue?vue&type=style&index=0&id=763db97b&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _HelloWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "763db97b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "components/HelloWorld.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./components/HelloWorld.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/babel-loader/lib/index.js!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./components/HelloWorld.vue?vue&type=style&index=0&id=763db97b&scoped=true&lang=css&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-dev-webpack/style-hot-loader.js!../node_modules/nativescript-dev-webpack/apply-css-loader.js!../node_modules/css-loader/index.js?!../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=style&index=0&id=763db97b&scoped=true&lang=css&");
/* harmony import */ var _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_nativescript_dev_webpack_style_hot_loader_js_node_modules_nativescript_dev_webpack_apply_css_loader_js_node_modules_css_loader_index_js_ref_1_2_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_style_index_0_id_763db97b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./components/HelloWorld.vue?vue&type=template&id=763db97b&scoped=true&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/vue-loader/lib/loaders/templateLoader.js?!../node_modules/vue-loader/lib/index.js?!./components/HelloWorld.vue?vue&type=template&id=763db97b&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HelloWorld_vue_vue_type_template_id_763db97b_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc","forceLog":true},"main":"app.js","name":"tns-template-vue","version":"3.2.0"};

/***/ }),

/***/ "nativescript-camera":
/***/ (function(module, exports) {

module.exports = require("nativescript-camera");

/***/ }),

/***/ "nativescript-geolocation":
/***/ (function(module, exports) {

module.exports = require("nativescript-geolocation");

/***/ }),

/***/ "nativescript-imagepicker":
/***/ (function(module, exports) {

module.exports = require("nativescript-imagepicker");

/***/ }),

/***/ "nativescript-ui-sidedrawer":
/***/ (function(module, exports) {

module.exports = require("nativescript-ui-sidedrawer");

/***/ }),

/***/ "nativescript-vue":
/***/ (function(module, exports) {

module.exports = require("nativescript-vue");

/***/ }),

/***/ "tns-core-modules/application":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/application");

/***/ }),

/***/ "tns-core-modules/bundle-entry-points":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/bundle-entry-points");

/***/ }),

/***/ "tns-core-modules/connectivity":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/connectivity");

/***/ }),

/***/ "tns-core-modules/image-source":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/image-source");

/***/ }),

/***/ "tns-core-modules/ui/styling/style-scope":
/***/ (function(module, exports) {

module.exports = require("tns-core-modules/ui/styling/style-scope");

/***/ })

/******/ });