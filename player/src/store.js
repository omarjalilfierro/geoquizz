import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {

        score: 0, //le score
        game: false, // partie en cours
        marker: false, //emplacement du marker posé sur la carte par le joueur
        start: false, //moment du départ du chrono
        chrono: false, //Le temps effectué à chaque photo
        currentLevel : false, //le niveau choisi pour la partie
        currentSeries : false, //la série choisie pour la partie
        isPlaying : false, //en train de jouer
        photoIndex:0, // index de la photo affichée
    },
    mutations: {
        setPhotoIndex(state, photoIndex){
            state.photoIndex = photoIndex;
        },
        setCurrentSeries(state, series){
            state.currentSeries = series;
        },
        setIsPlaying(state, isPlaying){
            state.isPlaying = isPlaying;
        },
        setCurrentLevel(state, level){
            state.currentLevel = level;
        },
        setStart(state, start){
            state.start = start;
        },
        setGame(state, game){
            state.game = game;
        },
        setChrono(state, chrono){
            state.chrono = chrono;
        },
        setScore(state, score){
            state.score = score;
        },
        setMarker(state, marker){
            state.marker = marker;
        },
        initialiseStore(state) {
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }

    }
})
