import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'
import {Utils} from './components/mixins/utils.js'
import { Icon } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import {LMap, LTileLayer, LMarker, LIcon, LTooltip, LPopup  } from 'vue2-leaflet';


Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-icon', LIcon);
Vue.component('l-tooltip', LTooltip);
Vue.component('l-popup', LPopup);

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

Vue.mixin(Utils); //methodes pour calculer la distance et commencer le timer
Vue.config.productionTip = false;


store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

window.axios = axios.create({
    baseURL: 'http://api.player.local:10080', //dans docker-compose
    headers: {
        Authorization: false
    }
});

new Vue({
    router,
    store,
    render: h => h(App),
    beforeCreate() {
        this.$store.commit('initialiseStore');
    }
}).$mount('#app');
