
export const Utils = {
    methods: {
        toRadian(degree) {
            return degree * Math.PI / 180;
        },

        //Calcule la distance en mètre entre 2 coordonnées
        getDistance(origin, destination) {
            let lon1 = this.toRadian(origin[1]);
            let lat1 = this.toRadian(origin[0]);
            let lon2 = this.toRadian(destination[1]);
            let lat2 = this.toRadian(destination[0]);

            let deltaLat = lat2 - lat1;
            let deltaLon = lon2 - lon1;

            let a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
            let c = 2 * Math.asin(Math.sqrt(a));
            let EARTH_RADIUS = 6371;
            return c * EARTH_RADIUS * 1000;
        },

        //Initialise le chrono de la manche (1 photo)
        startTimer() {
            let start = Date.now();
            this.$store.commit('setStart', start);
            this.$store.commit('setChrono', 0);

        },

        //Réinitialise les données du store
        reinitializeStore() {
            this.$store.commit('setStart', false);
            this.$store.commit('setGame', false);
            this.$store.commit('setChrono', false);
            this.$store.commit('setScore', 0);
            this.$store.commit('setMarker', false);
            this.$store.commit('setIsPlaying', false);
            this.$store.commit('setPhotoIndex', 0);
        }
    }
}