USE geoQuizz;

INSERT INTO `series` ( `city`, `lat`, `lng`, `zoom`) VALUES
('Nancy',	48.6876972,	6.1843538, 18	),
('Metz',	49.1090393,	6.1620759,	13);

INSERT INTO `photo` (`id`, `description`, `lat`, `lng`, `url`, `idSeries`) VALUES
(1,	'Place Stanislas',	48.6935300,	6.1831299,	'https://res.cloudinary.com/geoquizz/image/upload/v1552573155/stan.jpg',	1),
(2,	"Cathédrale Notre-Dame-de-l\'Annonciation",	48.6913239,	6.1859742,	'https://res.cloudinary.com/geoquizz/image/upload/v1552570265/cathedrale-notre-dame-de_l-anonciation.jpg',	1),
(3,	'Basilique Saint-Epvre',	48.6959738,	6.1797998,	'https://res.cloudinary.com/geoquizz/image/upload/v1552570256/basilique-saint-epvre.jpg',	1),
(4,	'Parc de la Pépinière',	48.6980560,	6.1828113,	'https://res.cloudinary.com/geoquizz/image/upload/v1552570532/pepiniere-park.jpg',	1),
(5,	'IUT Charlemagne',	48.6828315,	6.1609532,	'https://res.cloudinary.com/geoquizz/image/upload/v1552570531/iut_charlemagne.jpg',	1),
(6,	'Villa Majorelle',	48.6856139,	6.1640193,	'https://res.cloudinary.com/geoquizz/image/upload/v1552570299/Villa_Majorelle.jpg',	1),
(7,	'Kinepolis',	48.6916441,	6.1958308,	'https://res.cloudinary.com/geoquizz/image/upload/v1552571037/kinepolis.jpg',	1),
(8,	'Racecourse Nancy-Brabois',	48.6533587,	6.1424713,	'https://res.cloudinary.com/geoquizz/image/upload/v1552571018/hypodrome.jpg',	1),
(9,	'Porte de la Citadelle',	48.7001487,	6.1779779,	'https://res.cloudinary.com/geoquizz/image/upload/v1552571002/Nancy-Porte_de_la_Citadelle-001.jpg',	1),
(10,	'Stade Marcel Picot',	48.6956309,	6.2107914,	'https://res.cloudinary.com/geoquizz/image/upload/v1552572128/stade-Marcel-Picot-int%C3%A9rieur-ASNL-854x569.jpg',	1),
(11,	'Porte Désilles',	48.6978660,	6.1744900,	'https://res.cloudinary.com/geoquizz/image/upload/v1552572495/memorial_desilles_face_sud.jpg',	1),
(12,	'Château de Montaigu',	48.6634521,	6.2155350,	'https://res.cloudinary.com/geoquizz/image/upload/v1552572625/chateau.jpg',	1),
(13,	'Nancy Museum-Aquarium',	48.6948927,	6.1881627,	'https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg',	1),
(14,	'Maison de Jean Prouvé',	48.7005344,	6.1615899,	'https://res.cloudinary.com/geoquizz/image/upload/v1552573538/2006-04-maison-prouve.jpg',	1),
(15,	'Catedral de Metz',	49.1203475,	6.1757264,	'https://res.cloudinary.com/geoquizz/image/upload/v1552917408/Northern_Facade_Metz_Cathedral.jpg',	2),
(16,	'Temple Neuf',	49.1207015,	6.1718790,	'https://res.cloudinary.com/geoquizz/image/upload/v1552917617/Metz_temple_neuf.jpg',	2),
(17,	'Gare de Metz',	49.1097318,	6.1771878,	'https://res.cloudinary.com/geoquizz/image/upload/v1552917784/7066164lpw-7066257-sommaire-gare-metz-jpg_4106897.jpg',	2),
(18,	'Centre Pompidou-Metz',	49.1082314,	6.1814763,	'https://res.cloudinary.com/geoquizz/image/upload/v1552918037/el-centro-pompidou-madera-techo.jpg',	2),
(19,	'Porte des Allemands',	49.1178823,	6.1853177,	'https://res.cloudinary.com/geoquizz/image/upload/v1552918316/Metz-Porte_des_Allemands_032.jpg',	2),
(20,	'Museum of La Cour d\'Or',	49.1210565,	6.1780339,	'https://res.cloudinary.com/geoquizz/image/upload/v1552923479/musee-cour-dor-travaux-2016.jpg',	2);


INSERT INTO `user` (`id`, `email`,`password`) VALUES
('e273da32-f1d4-4c48-8542-449048dbf572', 'test@test.fr', '$2y$10$U3D9GxWYmdt3nLdT47sVz.DjxYfR1vKZ5.GKqZqsAvX1oTqWtKd5.')