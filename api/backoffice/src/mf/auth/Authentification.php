<?php
namespace geoQuizz\backoffice\mf\auth;

class Authentification extends AbstractAuthentification{

    /**
     * Vérification du niveau d'acces
     * @param mixed $requested : niveau d'acces requit
     * 
     * @return bool Si true acces autorisé, Sinon acces refusé
      */
    public function checkAccessRight($requested){
        if($requested > $this->access_level){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Hash le mot de passe
     * @param string $password : mot de passe
     * 
     * @return string mot de passe hashé
      */
    public static function hashPassword($password){
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Vérifie le mot de passe
     * @param string $password : mot de passe donné (en clair)
     * @param string $hash : mot de passe hashé
     * 
     * @return bool Si true mot de passe identique, Sinon mot de passe différent
      */
    public static function verifyPassword($password, $hash){
        return password_verify($password, $hash);
    }

    public static function getToken($req){
        if(isset($req->getHeader('Authorization')[0])){
            $header = $req->getHeader('Authorization')[0];
            if(!empty($header)){
                $tokenstring = sscanf($header, "Bearer %s")[0];
                if(!empty($tokenstring)){
                    $token = explode(".",$tokenstring);
                    $token[0] = json_decode(base64_decode($token[0]));
                    $token[1] = json_decode(base64_decode($token[1]));
                    $token[2] = json_decode(base64_decode($token[2]));
                    $token[3] = $tokenstring;
                    return $token;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}