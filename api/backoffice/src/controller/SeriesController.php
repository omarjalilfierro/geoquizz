<?php

namespace geoQuizz\backoffice\controller;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use geoQuizz\backoffice\response\Writter;
/* Errors */
use geoQuizz\backoffice\errors\NotAllowed;
use geoQuizz\backoffice\errors\NotFound;
use geoQuizz\backoffice\errors\PhpError;
/* Models */
use geoQuizz\backoffice\models\Series;
use geoQuizz\backoffice\models\Photo;

class SeriesController{

    protected $app;

    public function __construct($pApp){
        $this->app = $pApp;
    }

    /** Méthode get
     * Get a part of series (pagination) and his photos
     * Params : page
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Array series list
      */
    public function get(Request $request, Response $response, array $args){

        $limit = 5;
        if(!empty($request->getQueryParam('page'))){
            $page = $request->getQueryParam('page');
        }
        else{
            $page = 1;
        }
        $offset = (--$page) * $limit;
        $count = Series::all()->count();
        $lastPage = ceil($count / $limit);
        $listSeries = Series::skip($offset)->take($limit)->get();

        foreach ($listSeries as $oneSeries) {
            $oneSeries->photos;
        }

        $next = $page + 2;
        if($next > $lastPage){
            $next = $lastPage;
        }

        $prev = $page;
        if($prev == 0){
           $prev = 1; 
        }

        $data = [
            'count' => count($listSeries->toArray()),
            'series' => $listSeries,
            'page' => [
                'prev' => $prev,
                'next' => $next,
                'first' => 1,
                'last' => $lastPage
            ]
        ];

        return Writter::jsonSuccess($response, $data, 206, 'collection');
    }

    /** Méthode create
     * Create a new series with a minimum of 10 photos associate
     * (just share id of the photo)
     * Body : city, lat, lng, zoom, photos
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function create(Request $request, Response $response, array $args): Response{
        $body = $request->getParsedBody();
        $error = false;

        if(isset($body['city'])){
            $city = filter_var($body['city'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }

        if(isset($body['lat'])){
            $lat = filter_var($body['lat']);
        }
        else{
            $error = true;
        }

        if(isset($body['lng'])){
            $lng = filter_var($body['lng']);
        }
        else{
            $error = true;
        }

        if(isset($body['zoom'])){
            $zoom = filter_var($body['zoom'], FILTER_SANITIZE_NUMBER_INT);
        }
        else{
            $error = true;
        }

        if(isset($body['photos'])){
            $photos = $body['photos'];
            if(count($photos) >= 10){
                $allPhotos = [];
                foreach ($photos as $photo) {
                    $verifPhoto = Photo::find($photo);
                    if(!empty($verifPhoto)){
                        $allPhotos[] = $verifPhoto;
                    }
                    else{
                        $error = true;
                        return Writter::jsonError($response, "Not found photo : ".$photo, 404);
                    }
                }
            }
            else{
                $error = true;
                return Writter::jsonError($response, "Need 10 photos", 403);
            }
        }


        if(!$error){
            try{
                $series = new Series();
                $series->city = $city;
                $series->lat = $lat;
                $series->lng = $lng;
                $series->zoom = $zoom;
                $series->save();

                foreach ($allPhotos as $photo) {
                    $photo->series()->associate($series);
                    $photo->save();
                }
                $series->photos;
                return Writter::jsonSuccess($response, array('series' => $series), 201);
            }
            catch(\Exception $e){
                return PhpError::error($request, $response, $e);
            }
        }
        else{
            return Writter::jsonError($response, "Missing data", 403);
        }
    }

    /** Méthode save
     * Edit a part of a series named by an id in the uri
     * Body : city (optionnal), lat(optionnal), zoom (optionnal),
     * photos (optionnal, just share id of the photo, minimum of 10 photos)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
      */
    public function save(Request $request, Response $response, array $args): Response{
        $id = filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT);
        try{
            $series = Series::find($id);

            if(!empty($series)){
                $error = false;

                $body = $request->getParsedBody();

                if(isset($body['city'])){
                    $city = filter_var($body['city'], FILTER_SANITIZE_STRING);
                    $series->city = $city;
                }

                if(isset($body['lat'])){
                    $lat = filter_var($body['lat']);
                    $series->lat = $lat;
                }

                if(isset($body['lng'])){
                    $lng = filter_var($body['lng']);
                    $series->lng = $lng;
                }

                if(isset($body['zoom'])){
                    $zoom = filter_var($body['zoom'], FILTER_SANITIZE_NUMBER_INT);
                    $series->zoom = $zoom;
                }

                if(isset($body['photos'])){
                    $photos = $body['photos'];

                    if(count($photos) >= 10){
                        foreach ($series->photos as $photo) {
                            $photo->series()->dissociate();
                            $photo->save();
                        }

                        $allPhotos = [];

                        foreach ($photos as $id) {
                            $photo = Photo::find($id);
                            $photo->series()->associate($series);
                            $photo->save();
                        }
                    }
                    else{
                        $error = true;
                        return Writter::jsonError($response, "Need 10 photos", 403);
                    }
                }

                if(!$error){
                    try{
                        $series->save();

                        return Writter::jsonSuccess($response, array('success' => 1), 204);
                    }
                    catch(\Exception $e){
                        return PhpError::error($request, $response, $e);
                    }
                }
                else{
                    return Writter::jsonError($response, "Missing data", 403);
                }

            }
            else{
                return NotFound::error($request, $response);
            }
        }
        catch(\Exception $e){
            return PhpError::error($request, $response, $e);
        }
    }
}