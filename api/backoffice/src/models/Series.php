<?php

namespace geoQuizz\backoffice\models;
use Illuminate\Database\Eloquent\Model;

class Series extends Model{

    protected $table = 'series';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function photos(){
        return $this->hasMany(
            'geoQuizz\backoffice\models\Photo',
            'idSeries',
            'id'
        );
    }

    public function games(){
        return $this->hasMany(
            'geoQuizz\backoffice\models\Game',
            'idSeries',
            'id'
        );
    }

}