<?php

namespace geoQuizz\backoffice\models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model{

    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function series(){
        return $this->belongsTo(
            'geoQuizz\backoffice\models\Series',
            'idSeries');
    }

}