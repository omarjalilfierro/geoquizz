<?php

namespace geoQuizz\backoffice\models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function series(){
        return $this->belongsTo(
            'geoQuizz\backoffice\models\Series',
            'idSeries');
    }

    public function level(){
        return $this->belongsTo(
            'geoQuizz\backoffice\models\Level',
            'idLevel');
    }

}