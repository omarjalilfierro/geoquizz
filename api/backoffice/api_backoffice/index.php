<?php

use geoQuizz\backoffice\eloquent\Eloquent;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use geoQuizz\backoffice\response\Writter;
use geoQuizz\backoffice\mf\auth\Authentification;
use Firebase\JWT\JWT;
/* Errors */
use geoQuizz\backoffice\errors\NotAllowed;
use geoQuizz\backoffice\errors\NotFound;
use geoQuizz\backoffice\errors\PhpError;
/* Models */
use geoQuizz\backoffice\models\User;
/* Controllers */
use geoQuizz\backoffice\controller\SeriesController;
use geoQuizz\backoffice\controller\PhotoController;
use geoQuizz\backoffice\controller\UserController;
use geoQuizz\backoffice\controller\LevelController;


require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

$verifToken = function ($req, $resp, $next) {
    if(Authentification::getToken($req)){
        $token = Authentification::getToken($req);
        $user = User::find($token[1]->uid);
        if(!empty($user)){
            $key = $user->token;
            try{
                $token = JWT::decode($token[3], $key, ['HS512'] ) ;
                return $next($req, $resp);
            }
            catch (ExpiredException $e) {
                return PhpError::error($req, $resp, $e);
            } catch (SignatureInvalidException $e) {
                return PhpError::error($req, $resp, $e);
            } catch (BeforeValidException $e) {
                return PhpError::error($req, $resp, $e);
            } catch (\UnexpectedValueException $e) {
                return PhpError::error($req, $resp, $e);
            }
        }
        else{
            return Writter::jsonError($resp, "Bad Credentials", 401);
        }
    }
    else{
        return Writter::jsonError($resp, "Missing token", 403);
    }

    return $response;
};

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->get('[/]', function (Request $request, Response $response,array $args){
    $result = <<<EOT
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Api Backoffice</title>
        </head>
    
        <body>
            <h1>Api Backoffice</h1>
            <table>
                <tr>
                    <th>Route</th>
                    <th>Methode</th>
                    <th>Header params</th>
                    <th>Body params</th>
                </tr>
                <tr>
                    <td>/series[/]</td>
                    <td>GET</td>
                    <td>Authorization Bearer {token}</td>
                    <td>none</td>
                </tr>
                <tr>
                    <td>/series[/]</td>
                    <td>POST</td>
                    <td>Authorization Bearer {token}</td>
                    <td>city, lat, lng, dist, photos (array of idPhotos, min 10 photos)</td>
                </tr>
                <tr>
                    <td>/series/{id}[/]</td>
                    <td>PATCH</td>
                    <td>Authorization Bearer {token}</td>
                    <td>city, lat, lng, dist, photos (array of idPhotos, min 10 photos)</td>
                </tr>
                <tr>
                    <td>/photos[/]</td>
                    <td>GET</td>
                    <td>Authorization Bearer {token}</td>
                    <td>none</td>
                </tr>
                <tr>
                    <td>/photos[/]</td>
                    <td>POST</td>
                    <td>Authorization Bearer {token}</td>
                    <td>description, lat (can be null), lng (can be null), url, series (just id, can be null)</td>
                </tr>
                <tr>
                    <td>/photos/{id}[/]</td>
                    <td>PATCH</td>
                    <td>Authorization Bearer {token}</td>
                    <td>lat, lng, series (id)</td>
                </tr>
                <tr>
                    <td>/users/login[/]</td>
                    <td>POST</td>
                    <td>Authorization Basic</td>
                    <td>none</td>
                </tr>
                <tr>
                    <td>/users[/]</td>
                    <td>POST</td>
                    <td>none</td>
                    <td>email password</td>
                </tr>
                <tr>
                    <td>/users[/]</td>
                    <td>DELETE</td>
                    <td>Authorization Bearer {token}</td>
                    <td>none</td>
                </tr>
                <tr>
                    <td>/levels[/]</td>
                    <td>GET</td>
                    <td>Authorization Bearer {token}</td>
                    <td>none</td>
                </tr>
                <tr>
                    <td>/levels[/]</td>
                    <td>POST</td>
                    <td>Authorization Bearer {token}</td>
                    <td>name, dist, nbphotos</td>
                </tr>
            </table>
        </body>
        <style>
            td{
                padding:0.2em 1em;
                border-top: 1px solid black;
                border-right: 1px solid black;
            }
            td:last-child{
                text-align: center;
                border-right: none;
            }
        </style>
    </html>
EOT;
    $response->write("$result");
    return $response->withStatus(200)->withHeader('Content-Type', 'text/html') ;
});

/* Series */
/**
 * @api {get} /series/ Recuperer toutes les series
 * @apiGroup Series
 * @apiName getSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher toutes les series avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apisuccess (Succès : 200) {Object} Series collection Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Series": [
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/series[/]', 
function (Request $request, Response $response,array $args) {
    return (new SeriesController($this))->get($request, $response, $args);
}
)->add($verifToken);

/**
 * @api {post} /series/ Créer des series
 * @apiGroup Series
 * @apiName createSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet des créer des series avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apiParam {String} city Ville de la serie
 * @apiParam {Number} lat Latitude de la serie
 * @apiParam {Number} lng Longitude de la serie
 * @apiParam {Number} zoom Zoom de la serie
 * @apisuccess (Succès : 200) {String} message Message de succés
 * @apisuccess (Succès : 200) {Object} Series resource Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "New series added successfully",
 *        "series": [
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/series[/]', 
    function (Request $request, Response $response,array $args) {
            return (new SeriesController($this))->create($request, $response, $args);
    }
)->add($verifToken);

/**
 * @api {patch} /series/{id}[/] Modifier des series
 * @apiGroup Series
 * @apiName createSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet des modifier des series avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apiParam {Number} id L'id de la serie
 * @apiParam {String} city Ville de la serie
 * @apiParam {Number} lat Latitude de la serie
 * @apiParam {Number} lng Longitude de la serie
 * @apiParam {Number} zoom Zoom de la serie
 * @apiParam {Number} photo L'id de la photo de la serie
 * @apisuccess (Succès : 200) {Object} Series resource Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "series": [
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->patch('/series/{id}[/]', 
    function (Request $request, Response $response,array $args) {
            return (new SeriesController($this))->save($request, $response, $args);
    }
)->add($verifToken);

/* Photos */
/**
 * @api {get} /photos[/] Recuperer les photos
 * @apiGroup Photos
 * @apiName getPhotos
 * @apiVersion 1.0.0
 * @apiDescription Accès à une collection de type photos:
 * permet d'accéder à la représentation de la collection des photos désignée.
 * Retourne une représentation json de la collection, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apisuccess (Succès : 200) {Object} photos collection photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "photos": [
 *          {
 *              "id": 1,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          },
 *          {
 *              "id": 2,
 *              "description": "Place Stanislas",
 *              "lat": 48.69353,
 *              "lng": 6.1831299,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552573155/stan.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/photos[/]', 
    function (Request $request, Response $response,array $args) {
        return (new PhotoController($this))->get($request, $response, $args);
    }
)->add($verifToken);

/**
 * @api {post} /photos[/] Créer des photos
 * @apiGroup Photos
 * @apiName createPhotos
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de créer des photos.
 * Retourne une représentation json de la resource, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apiParam {String} description Description de la photo
 * @apiParam {Number} lat Latitude de la photo
 * @apiParam {Number} lng Longitude de la photo
 * @apiParam {String} url L'url de la photo
 * @apiParam {Number} idSeries L'id de la serie correspondant (optionnel)
 * @apisuccess (Succès : 200) {Object} photo resource photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "photos":
 *          {
 *              "id": 1,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/photos[/]', 
    function (Request $request, Response $response,array $args) {
            return (new PhotoController($this))->create($request, $response, $args);
    }
)->add($verifToken);

/**
 * @api {patch} /photos/{id}[/] Modifier des photos
 * @apiGroup Photos
 * @apiName modifierPhotos
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de modifier des photos.
 * Retourne une représentation json de la resource, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apiParam {String} description Description de la photo
 * @apiParam {Number} lat Latitude de la photo
 * @apiParam {Number} lng Longitude de la photo
 * @apiParam {String} url L'url de la photo
 * @apiParam {Number} idSeries L'id de la serie correspondant (optionnel)
 * @apisuccess (Succès : 200) {String} message Message de succés
 * @apisuccess (Succès : 200) {Object} photo resource photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "New photo added successfully",
 *        "photos":
 *          {
 *              "id": 1,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->patch('/photos/{id}[/]', 
    function (Request $request, Response $response,array $args) {
            return (new PhotoController($this))->save($request, $response, $args);
    }
)->add($verifToken);

/* User */
/**
 * @api {post} /users/login[/] Permet se logger
 * @apiGroup User
 * @apiName login
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet se logger avec une authorization basic
 * @apiHeader {String} email email de l'utilisateur
 * @apiHeader {String} pass mot de passe de l'utilisateur
 * @apisuccess (Succès : 200) {String} token resource token retournée
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "token":
 *          {
 *              "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.VFb0qJ1LRg_4ujbZoRMXnVkUgiuKq5KxWqNdbKq_G9Vvz-S1zZa9LPxtHWKa64zDl2ofkT8F6jBt_K4riU-fPg"
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/users/login[/]', 
    function (Request $request, Response $response,array $args) {
            return (new UserController($this))->login($request, $response, $args);
    }
);
/**
 * @api {post} /users[/] Créer un utilisateur
 * @apiGroup User
 * @apiName createUser
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet créer un utilisateur avec son email et son mot de passe
 * @apiParam {String} email email de l'utilisateur
 * @apiParam {String} pass mot de passe de l'utilisateur
 * @apisuccess (Succès : 200) {Object} user resource User retournée
 * @apisuccess (Succès : 200) {String} user.id L'id de l'utilisateur en uuid
 * @apisuccess (Succès : 200) {String} user.email Email de l'utilisateur
 * @apisuccess (Succès : 200) {String} user.password Mot de passe de l'utilisateur
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "user":
 *          {
 *              "id": "e273da32-f1d4-4c48-8542-449048dbf572",
 *              "email": "test@test.fr",
 *              "password": "$2y$10$U3D9GxWYmdt3nLdT47sVz.DjxYfR1vKZ5.GKqZqsAvX1oTqWtKd5.",
 *              "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.VFb0qJ1LRg_4ujbZoRMXnVkUgiuKq5KxWqNdbKq_G9Vvz-S1zZa9LPxtHWKa64zDl2ofkT8F6jBt_K4riU-fPg"
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/users[/]', 
    function (Request $request, Response $response,array $args) {
            return (new UserController($this))->register($request, $response, $args);
    }
);

/**
 * @api {delete} /users[/] Permet se deconnecter
 * @apiGroup User
 * @apiName logout
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet se deconnecter avec une authorization basic
 * @apiHeader {String} token Token bearer avec l'information de l'utilisateur
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->delete('/users[/]', 
    function (Request $request, Response $response,array $args) {
            return (new UserController($this))->logout($request, $response, $args);
    }
);

/* Level */
/**
 * @api {get} /levels[/] Recuperer des niveaux
 * @apiGroup Level
 * @apiName getLevel
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher des niveaux avec la distance maximale, et le nombre des photos
 * @apisuccess (Succès : 200) {Object} Level collection niveau retournée
 * @apiSuccess (Succès : 200) {Number} Level.id Identifiant du niveau
 * @apiSuccess (Succès : 200) {String} Level.name Nom du niveau
 * @apiSuccess (Succès : 200) {Number} Level.dist Distance maximale pour avoir des points
 * @apiSuccess (Succès : 200) {Number} Level.nbphotos Nombre des photos par niveau
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Level": [
 *          {
 *              "id": 3,
 *              "name": "Difficile",
 *              "dist": 500,
 *              "nbphotos": 10,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Level inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/levels[/]', 
    function (Request $request, Response $response,array $args) {
        return (new LevelController($this))->get($request, $response, $args);
    }
)->add($verifToken);

/**
 * @api {post} /levels[/] Créer des niveaux
 * @apiGroup Level
 * @apiName createLevel
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de créer des niveaux avec la distance maximale, et le nombre des photos
 * @apiParam {String} name Nom du niveau
 * @apiParam {Number} dist Distance maximale pour avoir des points
 * @apiParam {Number} nbphotos Nombre des photos par niveau
 * @apisuccess (Succès : 200) {Object} Level resource niveau retournée
 * @apiSuccess (Succès : 200) {Number} Level.id Identifiant du niveau
 * @apiSuccess (Succès : 200) {String} Level.name Nom du niveau
 * @apiSuccess (Succès : 200) {Number} Level.dist Distance maximale pour avoir des points
 * @apiSuccess (Succès : 200) {Number} Level.nbphotos Nombre des photos par niveau
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Level": [
 *          {
 *              "id": 3,
 *              "name": "Difficile",
 *              "dist": 500,
 *              "nbphotos": 10,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Level inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/levels[/]', 
    function (Request $request, Response $response,array $args) {
            return (new LevelController($this))->create($request, $response, $args);
    }
)->add($verifToken);

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

$app->run();