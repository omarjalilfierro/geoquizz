define({ "api": [
  {
    "type": "post",
    "url": "/levels[/]",
    "title": "Créer des niveaux",
    "group": "Level",
    "name": "createLevel",
    "version": "1.0.0",
    "description": "<p>Permet de créer des niveaux avec la distance maximale, et le nombre des photos</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Nom du niveau</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "dist",
            "description": "<p>Distance maximale pour avoir des points</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "nbphotos",
            "description": "<p>Nombre des photos par niveau</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Level",
            "description": "<p>resource niveau retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.id",
            "description": "<p>Identifiant du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Level.name",
            "description": "<p>Nom du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.dist",
            "description": "<p>Distance maximale pour avoir des points</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.nbphotos",
            "description": "<p>Nombre des photos par niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"Level\": [\n     {\n         \"id\": 3,\n         \"name\": \"Difficile\",\n         \"dist\": 500,\n         \"nbphotos\": 10,\n     }\n    ],\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Level inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Level"
  },
  {
    "type": "get",
    "url": "/levels[/]",
    "title": "Recuperer des niveaux",
    "group": "Level",
    "name": "getLevel",
    "version": "1.0.0",
    "description": "<p>Permet d'afficher des niveaux avec la distance maximale, et le nombre des photos</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Level",
            "description": "<p>collection niveau retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.id",
            "description": "<p>Identifiant du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Level.name",
            "description": "<p>Nom du niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.dist",
            "description": "<p>Distance maximale pour avoir des points</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Level.nbphotos",
            "description": "<p>Nombre des photos par niveau</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"Level\": [\n     {\n         \"id\": 3,\n         \"name\": \"Difficile\",\n         \"dist\": 500,\n         \"nbphotos\": 10,\n     }\n    ],\n   \"locale\": \"fr-FR\",\n   \"type\": \"collection\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Level inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Level"
  },
  {
    "type": "post",
    "url": "/photos[/]",
    "title": "Créer des photos",
    "group": "Photos",
    "name": "createPhotos",
    "version": "1.0.0",
    "description": "<p>Permet de créer des photos. Retourne une représentation json de la resource, incluant sa latitude, sa longitude, sa description, son url et l'id de la serie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>L'url de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idSeries",
            "description": "<p>L'id de la serie correspondant (optionnel)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "photo",
            "description": "<p>resource photo retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.url",
            "description": "<p>Url de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"photos\":\n     {\n         \"id\": 1,\n         \"description\": \"Nancy Museum-Aquarium\",\n         \"lat\": 48.6948927,\n         \"lng\": 6.1881627,\n         \"url\": \"https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg\",\n         \"idSeries\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "get",
    "url": "/photos[/]",
    "title": "Recuperer les photos",
    "group": "Photos",
    "name": "getPhotos",
    "version": "1.0.0",
    "description": "<p>Accès à une collection de type photos: permet d'accéder à la représentation de la collection des photos désignée. Retourne une représentation json de la collection, incluant sa latitude, sa longitude, sa description, son url et l'id de la serie.</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "photos",
            "description": "<p>collection photo retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.url",
            "description": "<p>Url de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"photos\": [\n     {\n         \"id\": 1,\n         \"description\": \"Nancy Museum-Aquarium\",\n         \"lat\": 48.6948927,\n         \"lng\": 6.1881627,\n         \"url\": \"https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg\",\n         \"idSeries\": 1\n     },\n     {\n         \"id\": 2,\n         \"description\": \"Place Stanislas\",\n         \"lat\": 48.69353,\n         \"lng\": 6.1831299,\n         \"url\": \"https://res.cloudinary.com/geoquizz/image/upload/v1552573155/stan.jpg\",\n         \"idSeries\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"collection\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "patch",
    "url": "/photos/{id}[/]",
    "title": "Modifier des photos",
    "group": "Photos",
    "name": "modifierPhotos",
    "version": "1.0.0",
    "description": "<p>Permet de modifier des photos. Retourne une représentation json de la resource, incluant sa latitude, sa longitude, sa description, son url et l'id de la serie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p>L'url de la photo</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "idSeries",
            "description": "<p>L'id de la serie correspondant (optionnel)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message de succés</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "photo",
            "description": "<p>resource photo retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.id",
            "description": "<p>Identifiant de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.description",
            "description": "<p>Description de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lat",
            "description": "<p>Latitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.lng",
            "description": "<p>Longitude de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "photo.url",
            "description": "<p>Url de la photo</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "photo.idSeries",
            "description": "<p>Id de la serie correspondant</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"message\": \"New photo added successfully\",\n   \"photos\":\n     {\n         \"id\": 1,\n         \"description\": \"Nancy Museum-Aquarium\",\n         \"lat\": 48.6948927,\n         \"lng\": 6.1881627,\n         \"url\": \"https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg\",\n         \"idSeries\": 1\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Photos"
  },
  {
    "type": "post",
    "url": "/series/",
    "title": "Créer des series",
    "group": "Series",
    "name": "createSeries",
    "version": "1.0.0",
    "description": "<p>Permet des créer des series avec la ville ou elle est, sa latitude, sa longitude et le zoom de la ville.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Ville de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "zoom",
            "description": "<p>Zoom de la serie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message de succés</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Series",
            "description": "<p>resource Series retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.id",
            "description": "<p>Identifiant de la serie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Series.city",
            "description": "<p>Le nom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lat",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lng",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.zoom",
            "description": "<p>Zoom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"message\": \"New series added successfully\",\n   \"series\": [\n     {\n         \"id\": 1,\n         \"city\": \"Nancy\",\n         \"lat\": 48.6876972,\n         \"lng\": 6.1843538,\n         \"zoom\": 18,\n     }\n    ],\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Series"
  },
  {
    "type": "patch",
    "url": "/series/{id}[/]",
    "title": "Modifier des series",
    "group": "Series",
    "name": "createSeries",
    "version": "1.0.0",
    "description": "<p>Permet des modifier des series avec la ville ou elle est, sa latitude, sa longitude et le zoom de la ville.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>L'id de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>Ville de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "zoom",
            "description": "<p>Zoom de la serie</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photo",
            "description": "<p>L'id de la photo de la serie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Series",
            "description": "<p>resource Series retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.id",
            "description": "<p>Identifiant de la serie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Series.city",
            "description": "<p>Le nom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lat",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lng",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.zoom",
            "description": "<p>Zoom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"series\": [\n     {\n         \"id\": 1,\n         \"city\": \"Nancy\",\n         \"lat\": 48.6876972,\n         \"lng\": 6.1843538,\n         \"zoom\": 18,\n     }\n    ],\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Series"
  },
  {
    "type": "get",
    "url": "/series/",
    "title": "Recuperer toutes les series",
    "group": "Series",
    "name": "getSeries",
    "version": "1.0.0",
    "description": "<p>Permet d'afficher toutes les series avec la ville ou elle est, sa latitude, sa longitude et le zoom de la ville.</p>",
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "Series",
            "description": "<p>collection Series retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.id",
            "description": "<p>Identifiant de la serie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "Series.city",
            "description": "<p>Le nom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lat",
            "description": "<p>Latitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.lng",
            "description": "<p>Longitude de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "Series.zoom",
            "description": "<p>Zoom de la ville</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"Series\": [\n     {\n         \"id\": 1,\n         \"city\": \"Nancy\",\n         \"lat\": 48.6876972,\n         \"lng\": 6.1843538,\n         \"zoom\": 18,\n     }\n    ],\n   \"locale\": \"fr-FR\",\n   \"type\": \"collection\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "Series"
  },
  {
    "type": "post",
    "url": "/users[/]",
    "title": "Créer un utilisateur",
    "group": "User",
    "name": "createUser",
    "version": "1.0.0",
    "description": "<p>Permet créer un utilisateur avec son email et son mot de passe</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email de l'utilisateur</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pass",
            "description": "<p>mot de passe de l'utilisateur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>resource User retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "user.id",
            "description": "<p>L'id de l'utilisateur en uuid</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "user.email",
            "description": "<p>Email de l'utilisateur</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "user.password",
            "description": "<p>Mot de passe de l'utilisateur</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"user\":\n     {\n         \"id\": \"e273da32-f1d4-4c48-8542-449048dbf572\",\n         \"email\": \"test@test.fr\",\n         \"password\": \"$2y$10$U3D9GxWYmdt3nLdT47sVz.DjxYfR1vKZ5.GKqZqsAvX1oTqWtKd5.\",\n         \"token\": \"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.VFb0qJ1LRg_4ujbZoRMXnVkUgiuKq5KxWqNdbKq_G9Vvz-S1zZa9LPxtHWKa64zDl2ofkT8F6jBt_K4riU-fPg\"\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/login[/]",
    "title": "Permet se logger",
    "group": "User",
    "name": "login",
    "version": "1.0.0",
    "description": "<p>Permet se logger avec une authorization basic</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email de l'utilisateur</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "pass",
            "description": "<p>mot de passe de l'utilisateur</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>resource token retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici resource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"token\":\n     {\n         \"token\": \"eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.VFb0qJ1LRg_4ujbZoRMXnVkUgiuKq5KxWqNdbKq_G9Vvz-S1zZa9LPxtHWKa64zDl2ofkT8F6jBt_K4riU-fPg\"\n     }\n   \"locale\": \"fr-FR\",\n   \"type\": \"resource\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/users[/]",
    "title": "Permet se deconnecter",
    "group": "User",
    "name": "logout",
    "version": "1.0.0",
    "description": "<p>Permet se deconnecter avec une authorization basic</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Token bearer avec l'information de l'utilisateur</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Serie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api_backoffice/index.php",
    "groupTitle": "User"
  }
] });
