define({
  "name": "BackOffice",
  "version": "1.0.0",
  "description": "Documentation de l'API BackOffice",
  "title": "Documentation API BackOffice",
  "url": "http://api.backoffice.local:30080",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-03-20T16:29:25.598Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
