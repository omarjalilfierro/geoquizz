<?php

use geoQuizz\mobile\controller\PhotoController;
use geoQuizz\mobile\controller\SeriesController;
use geoQuizz\mobile\eloquent\Eloquent;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;

require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

/**
 * @api {get} /photos[/] Recuperer les photos
 * @apiGroup Photos
 * @apiName getPhotos
 * @apiVersion 1.0.0
 * @apiDescription Accès à une collection de type photos:
 * permet d'accéder à la représentation de la collection des photos désignée.
 * Retourne une représentation json de la collection, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apisuccess (Succès : 200) {Object} photos collection photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "photos": [
 *          {
 *              "id": 1,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          },
 *          {
 *              "id": 2,
 *              "description": "Place Stanislas",
 *              "lat": 48.69353,
 *              "lng": 6.1831299,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552573155/stan.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/photos[/]',
    function (Request $req, Response $resp, array $args) {
        return (new PhotoController($this))->get($req, $resp, $args);
    }
)->setName('getPhotos');

/**
 * @api {get} /series/ Recuperer toutes les series
 * @apiGroup Series
 * @apiName getSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet d'afficher toutes les series avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apisuccess (Succès : 200) {Object} Series collection Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "Series": [
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "collection"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/series[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SeriesController($this))->get($req, $resp, $args);
    }
)->setName('getSeries');

/**
 * @api {post} /photos[/] Créer des photos
 * @apiGroup Photos
 * @apiName createPhotos
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet de créer des photos.
 * Retourne une représentation json de la resource, incluant sa latitude, sa longitude,
 * sa description, son url et l'id de la serie.
 * @apiParam {String} description Description de la photo
 * @apiParam {Number} lat Latitude de la photo
 * @apiParam {Number} lng Longitude de la photo
 * @apiParam {String} url L'url de la photo
 * @apiParam {Number} idSeries L'id de la serie correspondant (optionnel)
 * @apisuccess (Succès : 200) {String} message Message de succés
 * @apisuccess (Succès : 200) {Object} photo resource photo retournée
 * @apiSuccess (Succès : 200) {Number} photo.id Identifiant de la photo
 * @apiSuccess (Succès : 200) {String} photo.description Description de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lat Latitude de la photo
 * @apiSuccess (Succès : 200) {Number} photo.lng Longitude de la photo
 * @apiSuccess (Succès : 200) {String} photo.url Url de la photo
 * @apiSuccess (Succès : 200) {Number} photo.idSeries Id de la serie correspondant
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "New photo added successfully",
 *        "photos":
 *          {
 *              "id": 1,
 *              "description": "Nancy Museum-Aquarium",
 *              "lat": 48.6948927,
 *              "lng": 6.1881627,
 *              "url": "https://res.cloudinary.com/geoquizz/image/upload/v1552572777/aquarium.jpg",
 *              "idSeries": 1
 *          }
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/photos[/]',
    function (Request $req, Response $resp, array $args) {
        return (new PhotoController($this))->create($req, $resp, $args);
    }
)->setName('createPhoto');

/**
 * @api {post} /series/ Créer des series
 * @apiGroup Series
 * @apiName createSeries
 * @apiVersion 1.0.0
 * @apiDescription
 * Permet des créer des series avec la ville ou elle est, sa latitude, sa longitude
 * et le zoom de la ville.
 * @apiParam {String} city Ville de la serie
 * @apiParam {Number} lat Latitude de la serie
 * @apiParam {Number} lng Longitude de la serie
 * @apiParam {Number} zoom Zoom de la serie
 * @apisuccess (Succès : 200) {String} message Message de succés
 * @apisuccess (Succès : 200) {Object} Series resource Series retournée
 * @apiSuccess (Succès : 200) {Number} Series.id Identifiant de la serie
 * @apiSuccess (Succès : 200) {String} Series.city Le nom de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lat Latitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.lng Longitude de la ville
 * @apiSuccess (Succès : 200) {Number} Series.zoom Zoom de la ville
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici resource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "message": "New series added successfully",
 *        "series": [
 *          {
 *              "id": 1,
 *              "city": "Nancy",
 *              "lat": 48.6876972,
 *              "lng": 6.1843538,
 *              "zoom": 18,
 *          }
 *         ],
 *        "locale": "fr-FR",
 *        "type": "resource"
 *     }
 * @apiError (Erreur : 404) NotFound Serie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->post('/series[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SeriesController($this))->create($req, $resp, $args);
    }
)->setName('createSeries');

try {
    $app->run();
} catch (MethodNotAllowedException $e) {
    echo "MethodNotAllowedException error" . $e->getMessage();
} catch (NotFoundException $e) {
    echo "NotFoundException error" . $e->getMessage();
} catch (\Exception $e) {
    echo "Exception error" . $e->getMessage();
}