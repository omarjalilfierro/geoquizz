<?php

namespace geoQuizz\mobile\models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function series(){
        return $this->belongsTo(
            'geoQuizz\mobile\models\Series',
            'idSeries');
    }

}