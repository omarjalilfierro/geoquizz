<?php

namespace geoQuizz\mobile\models;
use Illuminate\Database\Eloquent\Model;

class Series extends Model{

    protected $table = 'series';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['city', 'lat', 'lng', 'zoom'];


    public function photos(){
        return $this->hasMany(
            'geoQuizz\mobile\models\Photo',
            'idSeries',
            'id'
        );
    }

    public function games(){
        return $this->hasMany(
            'geoQuizz\mobile\models\Game',
            'idSeries',
            'id'
        );
    }

}