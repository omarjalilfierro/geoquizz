<?php

namespace geoQuizz\player\models;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model{

    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function series(){
        return $this->belongsTo(
            'geoQuizz\player\models\Series',
            'idSeries');
    }

}