<?php

namespace geoQuizz\player\models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['status','player','idSeries','idLevel'];

    public function series(){
        return $this->belongsTo(
            'geoQuizz\player\models\Series',
            'idSeries');
    }

    public function level(){
        return $this->belongsTo(
            'geoQuizz\player\models\Level',
            'idLevel');
    }


}