<?php

namespace geoQuizz\player\controllers;

use geoQuizz\player\errors\NotFound;
use geoQuizz\player\errors\PhpError;
use geoQuizz\player\models\Level;
use geoQuizz\player\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LevelController{




    /** Méthode get
     * Get a level if id is in the request body
     * OR get all levels
     * Body : id (optionnal)
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Array liste de levels ou level
     */
    public function getLevels(Request $req, Response $resp, array $args): Response {
        try {

            if (isset($args['id'])) {
                $idLevel = $args['id'];
                $level =  Level::findOrFail($idLevel);
                return Writter::jsonSuccess($resp, $level, 200, 'resource');

            } else {
                $levels = Level::all();
                return Writter::jsonSuccess($resp, ["levels"=>$levels], 200, 'collection');
            }
        } catch (\Exception $exception) {
            return NotFound::Error($req, $resp);
        }
    }

}