<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 3/11/19
 * Time: 3:46 PM
 */
namespace geoQuizz\player\controllers;

use geoQuizz\player\errors\NotFound;
use geoQuizz\player\errors\PhpError;
use geoQuizz\player\models\Photo;
use geoQuizz\player\models\Series;
use geoQuizz\player\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class SeriesController{


    /** Methode getPhotos
     * Get all photos of a series
     * OR get nb photos of a series
     * Body : id
     * Params : nb (optionnal)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getPhotos(Request $req, Response $resp, array $args): Response
    {
        try {
            $idSeries = $args['id'];
            //On decide combien de photos on veut afficher dans le resultat
            $nbPhotos = isset($req->getQueryParams()['nb']) ? $req->getQueryParams()['nb'] : 10;
            $photos = Photo::inRandomOrder()->where('idSeries',$idSeries)->limit($nbPhotos)->get();
            $data = [
                'count' => count($photos),
                'photos' => $photos
            ];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');
        } catch (\Exception $exception) {
            return NotFound::error($req,$resp);
        }
    }

    /** Méthode get
     * Get a Series if id is in the request body
     * OR get all series
     *
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Array liste de series ou serie
     */
     public function getSeries(Request $req, Response $resp, array $args): Response {
         try {

             if (isset($args['id'])) {
                 $idSeries = $args['id'];
                 $aSeries =  Series::findOrFail($idSeries);
                 return Writter::jsonSuccess($resp, $aSeries, 200, 'resource');

             } else {
                 $series = Series::all();
                 return Writter::jsonSuccess($resp, ["series"=>$series], 200, 'collection');
             }
         } catch (\Exception $exception) {
             return PhpError::Error($req, $resp, $exception->getMessage());
         }
    }

}