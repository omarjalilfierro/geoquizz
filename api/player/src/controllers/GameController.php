<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 3/11/19
 * Time: 5:16 PM
 */

namespace geoQuizz\player\controllers;

use geoQuizz\player\errors\NotFound;
use geoQuizz\player\errors\PhpError;
use geoQuizz\player\models\Game;
use geoQuizz\player\models\Series;
use geoQuizz\player\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class GameController{

    /** Methode create
     * Create a new game
     * Body : status, player, idSeries, idLevel (optionnal)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function create(Request $req, Response $resp, array $args): Response{
        try{
            $data = $req->getParsedBody();
            //fillable dans le model. Pas besoin de SANITIZE eloquent le fait automatiquement
            $status = $data['status'];
            $player = $data['player'];
            $series = $data['idSeries'];
            $level = $data['idLevel'];

            if (isset($status) && isset($player) && isset($series)){
                $game = Game::create([
                    'status' => $status,
                    'player' => $player,
                    'idSeries' => $series,
                    'idLevel'  => $level
                    ]);
                return Writter::jsonSuccess($resp, array('games' => $game),200);
            }
        }catch (\Exception $exception) {
            return PhpError::error($req,$resp, $exception->getMessage());
        }
    }

    /**
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function index(Request $req, Response $resp, array $args): Response{
        try{
            if (isset($args['id'])){
                if ($args['id'] == 'best'){
                   try{
                       //Affiche les 15 meilleurs parties de la BDD
                       $games = Game::where('status',2)->orderBy('score','desc')->take(15)->get();
                       return Writter::jsonSuccess($resp, array('games' => $games),200);
                   }catch (\Exception $exception) {
                       return PhpError::error($req,$resp, $exception->getMessage());
                   }
                }
                //Sinon on cherche un jeu avec l'id
                $game = Game::findOrFail($args['id']);
                return Writter::jsonSuccess($resp, array('games' => $game),200);
            }
        }catch (\Exception $exception) {
            return NotFound::error($req,$resp);
        }
    }

    /** Methode update
     * Update score and status
     * Body : id, score (optionnal), status (optionnal)
     * 
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function update(Request $req, Response $resp, array $args): Response{
        try{
            $data = $req->getParsedBody();
            if (isset($args['id'])){
                $game = Game::findOrFail($args['id']);
            }else{
                return PhpError::error($req,$resp, 'Pas d\'id');
            }

            if (isset($data['score'])){//Si il y a score, on le met dans la bdd
                $game->score = $data['score'];
            }
            if (isset($data['status'])){//Si il y a status, on le met dans la bdd
                $game->status = $data['status'];//0 = pause //1 = en train de jouer // 2 = fini
            }

            $game->save();
            return Writter::jsonSuccess($resp, array('games' => $game),200);

        }catch (\Exception $exception) {
            return NotFound::error($req,$resp);
        }
    }

}