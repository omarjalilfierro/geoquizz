# GeoQuizz
Currently porting the game to Vue + Bulma + Laravel + Flutter https://github.com/RubenConde/geoquizzWeb

![alt text](https://res.cloudinary.com/geoquizz/image/upload/v1553089716/GeoQuizz.png)

# Game made with 3 API, docker containers, VueJs with axios. And a mobile App made with NativeScript



## Install Container Docker (docker-compose)
```
docker-compose -f docker-compose-yml up
```

### Install database (script in sql dir)

## Run a VueJs project
### Player project in player folder
### Mobile project in mobile folder
### Backoffice project in backoffice folder
```
npm install
npm run serve
```
