export const Utils = {
    methods: {

        //Ajoute le token aux requêtes axios
        setToken(token){
            axios.defaults.headers.Authorization = "Bearer "+token;
        },

        //Verifie si l'utilisateur est connecté
        memberConnected(){
            if(this.$store.state.token === false){
                return false;
            }
            else{
                this.setToken(this.$store.state.token);
                return true;
            }
        },
    }
}