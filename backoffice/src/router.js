import Vue from 'vue'
import Router from 'vue-router'
import Signin from './views/Signin.vue'
import Register from './views/Register.vue'
import ListSeries from './components/series/ListSeries.vue'
import ListPhotos from './components/photo/ListPhotos.vue'
import ListLevels from './components/level/ListLevels.vue'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Signin',
            component: Signin
        },
        {
            path: '/register',
            name: 'Register',
            component: Register
        },
        {
            path: '/series',
            name: 'Series',
            component: ListSeries
        },
        {
            path: '/photos',
            name: 'Photos',
            component: ListPhotos
        },
        {
            path: '/levels',
            name: 'Levels',
            component: ListLevels
        },
        // {
        //   path: '/about',
        //   name: 'about',
        //   // route level code-splitting
        //   // this generates a separate chunk (about.[hash].js) for this route
        //   // which is lazy-loaded when the route is visited.
        //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        // }
    ]
})
