import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import 'materialize-css'
import 'materialize-css/dist/css/materialize.css'
import {Utils} from './components/mixins/utils.js'
import { Icon } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import {LMap, LTileLayer, LMarker } from 'vue2-leaflet';
// import "./styles/global.scss";

/* Initialize leaflet */
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
    iconUrl: require('leaflet/dist/images/marker-icon.png'),
    shadowUrl: require('leaflet/dist/images/marker-shadow.png')
});

Vue.mixin(Utils);
Vue.config.productionTip = false;

/* Get the current localStorage */
store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

/* Initialize Axios */
window.axios = axios.create({
    baseURL: 'http://api.backoffice.local:30080',
    headers: {
      Authorization: false
    }
});

new Vue({
    router,
    store,
    render: h => h(App),
    beforeCreate() {
        this.$store.commit('initialiseStore');
    }
}).$mount('#app');
